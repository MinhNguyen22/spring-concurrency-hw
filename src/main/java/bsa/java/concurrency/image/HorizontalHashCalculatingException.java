package bsa.java.concurrency.image;

public class HorizontalHashCalculatingException extends RuntimeException {

    public HorizontalHashCalculatingException() {
        super();
    }

    public HorizontalHashCalculatingException(String message) {
        super(message);
    }

    public HorizontalHashCalculatingException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public HorizontalHashCalculatingException(Throwable throwable) {
        super(throwable);
    }


}
